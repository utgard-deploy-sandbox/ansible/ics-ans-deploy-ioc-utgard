#!/usr/bin/env python
import sys
import ast
import re

# running service names
services = ast.literal_eval(sys.argv[1])
# iocs data structure defining the iocs specified to run on the host
iocs_struct = ast.literal_eval(sys.argv[2])

# get just the name part of the running ioc services
search = re.compile("ioc@(.+)\.service").search;
running_iocs = [match.group(1) for match in map(search, services) if match]

# get just the names of the specified iocs
specified_iocs = [item["ioc"] for item in iocs_struct]

# get a list of all running iocs which are not in the set of specified iocs
to_be_removed = list(set(running_iocs) - set(specified_iocs))

# print this list to return it to ansible
print to_be_removed
